BlobApp = {};
BlobApp.Blobs=(function($){
	 var pub = {};

	 pub.init = function () {    
     pub.loadBlobs();
      $("#btnModal").click(function(e){
          pub.clear(); 
      });
      
    }
    pub.clear=function(){
      $("#inputName").val("");
          $("#inputBadge").val("");
          $("#inputIsLoved").prop( "checked", false );
          $("#inputDob").val(""); 
    }
     pub.getBlobsById = function (id){
      pub.editblob();
     	 $.ajax({
            type: "GET",
            url: "/blobs/blobsList",    
            dataType: "json"
          }).done(function( val ) {  
               var html = "";
               var blobs;
               $(".main-container-blobs tbody").html("");
               for(var i=0; i < val.length; i++){
                    blobs = val[i];
                if(blobs._id==id)
              	 {
              	 	$("#inputId").val(id);
               		$("#inputName").val(blobs.name);
               		$("#inputBadge").val(blobs.badge);
                  $("#inputIsLoved").prop("checked",blobs.isloved).is(':checked') ? 1 : 0;
                  $("#inputDob").val(blobs.dob);
              	 }     
               }
          });
    }
    pub.loadBlobs = function(){
    	 $.ajax({
            type: "GET",
            url: "/blobs/blobsList",    
            dataType: "json"
          })
          .done(function( val ) {  
               var html = "";
               var blobs;
               $(".main-container-blobs").html("");

               for(var i=0; i < val.length; i++){
                    blobs = val[i];
                    html = 
                    "<tr>"+
                    "<td>" + blobs._id + "</td>" +
                     "<td>" + blobs.name + "</td>"+ 
                    "<td>" + blobs.badge + "</td>" +
                    "<td>" + blobs.isloved + "</td>"+
                     "<td>" + blobs.dob + "</td>"+
                    "<td> <a class='btn btn-mini' style='cursor:pointer' id='linkedit' data-toggle='modal' data-target='#myModal' onclick='blobs.getBlobsById(&quot;"+blobs._id+"&quot;)' > edit</a></td>"+
                    "<td ><a style='cursor:pointer' onclick='blobs.delete(&quot;"+blobs._id+"&quot;)'>delete</a></td></br>"
                 "</tr>"; 
                 $(".main-container-blobs").append(html);      
               }    
          });
    }

    pub.editBlobs = function(id){
	    var blobs = {};
      var valid=true;
	     blobs.name = $("#inputName").val();
	     blobs.badge = $("#inputBadge").val();
	     blobs.dob = $("#inputDob").val();
	     blobs.isloved = $("#inputIsLoved").is(':checked');
	     blobs._id = $("#inputId").val();
	 	   
       valid1=pub.valid(blobs.name);
       valid2=pub.valid(blobs.badge);
       valid3=pub.valid(blobs.dob);
	     blobs._method = "PUT";
       if((valid1)&&(valid2)&&(valid3)){
	     $.ajax({
    	    type: "POST",
    	    url: "/blobs/"+blobs._id+"/edit",        
    	    data    : blobs, //forms user object
    	  }).done(function( val ) {
         $(".susscessful").show();
         $(".susscessful").delay(1000).hide(1600);
         $("#myModal").modal('hide');
        pub.loadBlobs(); 
	    }).fail(function(datas) {
	     alert( "error" );
	   });
      }else{return;}
	  }
    pub.valid=function(x)
    {
      if(x=="")
      {
        $("#allfields").show();
        $("#allfields").delay(1000).hide(1600);
      }else return true;
    }
    pub.newBlob = function(){
     $("#btnInsert").attr("onclick", "blobs.insertBlobs()")
     
    }
    pub.editblob = function(){
     $("#btnInsert").attr("onclick", "blobs.editBlobs()");
     $("#inputDob").attr("type", "text")
     
    }
    pub.insertBlobs=function(){
      var blob = {};
      var valid=true;
      var 
      name=$("#inputName").val(),
      badge=$("#inputBadge").val(),
      dob=$("#inputDob").val(),
      isloved=$("#inputIsLoved").is(':checked');
      valid1 = pub.valid(name);
      valid2 = pub.valid(badge);
      valid3 = pub.valid(dob);
      if((valid1)&&(valid2)&&(valid3)){
        blob.name = name;
        blob.badge = badge;
        blob.dob = dob;
        blob.isloved = isloved;
        blob._id = $("#inputId").val();     
        $.ajax({
          type: "POST",
          url: "/blobs",        
          data    : blob, //forms user object
        }).done(function( datas ) {
          $(".alert").show();
          $(".alert").delay(1000).hide(1600);
          $("#myModal").modal('hide');
          pub.loadBlobs();   
        }).fail(function() {
          alert( "error" );
      });
    }else{return;}
  }
  pub.delete=function(blobId){
    var answer = confirm("Deseas eliminar este registro?");
    if (answer){
      $.ajax({
      type: "POST",
      url: "/blobs/"+blobId+"/edit",    
      dataType: "json",
      data    : {'_method':"DELETE"}, //forms user object
    }).done(function( datas ) { 
        $(".alert").show();
        $(".alert").delay(1000).hide(1600); 
        $(".main-container-blobs tbody #blob-"+blobId).remove(); 
       pub.loadBlobs();
  }).fail(function() {
   alert( "error" );
    });
  }else {return;}
  }
    return pub; 
})(jQuery);
jQuery(BlobApp.Blobs.init);
var blobs = BlobApp.Blobs;


